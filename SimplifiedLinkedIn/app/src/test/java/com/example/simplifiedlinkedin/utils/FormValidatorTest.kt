package com.example.simplifiedlinkedin.utils

import org.junit.Assert.*
import org.junit.Test

/*
 *
 * Test suite for Form Validator
 */
class FormValidatorTest{

    @Test
    fun test_mobile_no_empty_returns_MOBILE_EMPTY_error(){
        assertEquals(FormErrors.MOBILE_EMPTY, FormValidator.isInputValid( "", "1234"))
    }

    @Test
    fun test_mobile_no_less_than10_returns_MOBILE_LENGTH_LESS_THAN_10_error(){
        assertEquals(FormErrors.MOBILE_LENGTH_LESS_THAN_10, FormValidator.isInputValid( "99884", "1234"))
    }

    @Test
    fun test_mobile_no_starting_with_0_returns_MOBILE_STARTS_WITH_0_error(){
        assertEquals(FormErrors.MOBILE_STARTS_WITH_0, FormValidator.isInputValid( "0998841234", "1234"))
    }

    @Test
    fun test_mobile_no_not_numerical_returns_MOBILE_NOT_NUMERICAL_error(){
        assertEquals(FormErrors.MOBILE_NOT_NUMERICAL, FormValidator.isInputValid("9988,%89911", "1234"))
    }

    @Test
    fun test_password_empty_returns_PASSWORD_EMPTY_error(){
        assertEquals(FormErrors.PASSWORD_EMPTY, FormValidator.isInputValid("9988776655", ""))
    }

    @Test
    fun test_password_less_than_4_characters_returns_PASSWORD_SHORT_error(){
        assertEquals(FormErrors.PASSWORD_SHORT, FormValidator.isInputValid("9988776655", "ab1"))
    }

    @Test
    fun test_valid_input_returns_NO_ERROR(){
        assertEquals(FormErrors.NO_ERROR, FormValidator.isInputValid("9988776655", "abc123"))
    }
}