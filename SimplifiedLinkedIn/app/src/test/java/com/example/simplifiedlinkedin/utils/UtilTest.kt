package com.example.simplifiedlinkedin.utils

import android.content.Intent
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito

/*
 *
 * utils test cases
 */
class UtilTest {

    @Test
    public fun test_containValues_when_intent_null_returns_false(){
        val intent = Mockito.mock(Intent::class.java)
        Assert.assertFalse("Null", intent.containsValue("ABC"))
    }

}