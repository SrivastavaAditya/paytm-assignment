package com.example.simplifiedlinkedin.repositories

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.simplifiedlinkedin.MyApplication
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.models.FeedResponse
import com.example.simplifiedlinkedin.network.RetrofitClient
import com.example.simplifiedlinkedin.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class DashboardRepository {

    companion object{
        const val TAG = "DashboardRepository"
    }


    /*
     *
     * method to fetch feed data
     */
    fun getFeedData(): MutableLiveData<FeedResponse>{
        var liveData = MutableLiveData<FeedResponse>()

        try{
            if(isInternetConnected()){
                val call: Call<FeedResponse> = RetrofitClient.getDashboardClient().getFeedData(Constants.DASHBOARD_URL)
                call.enqueue(object : Callback<FeedResponse>{
                    override fun onFailure(call: Call<FeedResponse>, t: Throwable) {
                        val feedResponse = FeedResponse()
                        feedResponse.status = "FAILED"
                        feedResponse.error = MyApplication.getInstance()!!.resources.getString(R.string.something_went_wrong)
                        liveData.value = feedResponse
                        showToastMessage(t.message.toString(), Toast.LENGTH_SHORT)
                    }

                    override fun onResponse(call: Call<FeedResponse>, response: Response<FeedResponse>) {
                        liveData.value = response.body()
                    }
                })
            }else{
                val feedResponse = FeedResponse()
                feedResponse.status = "FAILED"
                feedResponse.error = MyApplication.getInstance()!!.resources.getString(R.string.internet_conection_failed)
                liveData.value = feedResponse
                showToastMessage(MyApplication.getInstance()!!.resources.getString(R.string.internet_conection_failed)
                        , Toast.LENGTH_SHORT)
            }
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
            liveData.value = null
            showToastMessage(MyApplication.getInstance()!!.resources.getString(R.string.something_went_wrong)
                , Toast.LENGTH_SHORT)
        }

        return liveData
    }
}