package com.example.simplifiedlinkedin.dialogs

import android.app.Dialog
import android.content.Context
import com.example.simplifiedlinkedin.R


/*
 *
 * Loading dialog
 * ... displays during network service calls
 */
class LoadingDialog(context: Context) {
    //Dialog object
    var dialog: Dialog = Dialog(context)

    /*
     *
     * method to show Loader
     */
    private fun showLoader(){
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(R.layout.loading_dialog)
    }


    companion object {
        /*
         *
         * method to create Dialog object
         */
        fun getInstance(context: Context): LoadingDialog{
            return LoadingDialog(context)
        }
    }

    init {
        showLoader()
    }

    /*
     *
     * method to show dialog
     */
    fun show() {
        dialog.show()
    }


    /*
     *
     * method to dismiss dialog
     */
    fun dismiss() {
        if(dialog.isShowing){
            dialog.dismiss()
        }
    }

}