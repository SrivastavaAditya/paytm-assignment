package com.example.simplifiedlinkedin.activities

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.simplifiedlinkedin.MyApplication
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.adapters.PostsAndFeedsAdapter
import com.example.simplifiedlinkedin.dialogs.LoadingDialog
import com.example.simplifiedlinkedin.interfaces.PostClickListener
import com.example.simplifiedlinkedin.models.Post
import com.example.simplifiedlinkedin.models.UserModel
import com.example.simplifiedlinkedin.utils.*
import com.example.simplifiedlinkedin.viewmodels.DashboardViewModel
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.layout_no_data.*
import java.lang.Exception

/*
 *
 * Dashboard Activity
 */
class DashboardActivity : AppCompatActivity(), View.OnClickListener {

    companion object{
        const val TAG = "DashboardActivity"

        val currentUser: UserModel = getGson().fromJson(PrefUtils.getString(MyApplication.getInstance()!!,
                Constants.PREF_USER_DETAILS, null), UserModel::class.java)
    }

    lateinit var pAdapter: PostsAndFeedsAdapter
    lateinit var fAdapter: PostsAndFeedsAdapter

    var myPostsList = arrayListOf<Post>()
    var myFeedsList = arrayListOf<Post>()

    lateinit var dashboardViewModel: DashboardViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        dashboardViewModel = ViewModelProvider(this@DashboardActivity).get(DashboardViewModel::class.java)

        setViewsProps()

        setViewData()

        fetchFeedData()
    }


    /*
     *
     * method to fetch Feed
     */
    private fun fetchFeedData() {
        try{
            val dialog = LoadingDialog.getInstance(this@DashboardActivity)
            dialog.show()
            dashboardViewModel.getFeedData().observe(this@DashboardActivity,
                    Observer {
                        dialog.dismiss()
                        if(it != null){
                            if("SUCCESS".equals(it.status, ignoreCase = true) && TextUtils.isEmpty(it.error)){
                                if(it.data.isNotEmpty()){
                                    rl_error.visibility = View.GONE
                                    for(i in 0 until it.data.size){
                                        if(it.data[i].userId == currentUser.userId){
                                            myPostsList.add(it.data[i])
                                        }else{
                                            myFeedsList.add(it.data[i])
                                        }
                                    }

                                    pAdapter.list.clear()
                                    pAdapter.list = myPostsList
                                    pAdapter.notifyDataSetChanged()

                                    fAdapter.list.clear()
                                    fAdapter.list = myFeedsList
                                    fAdapter.notifyDataSetChanged()
                                }else{
                                    rl_error.visibility = View.VISIBLE
                                    tv_error_message.text = resources.getString(R.string.no_data_available)
                                }
                            }else{
                                rl_error.visibility = View.VISIBLE
                                tv_error_message.text = if(!TextUtils.isEmpty(it.error)) it.error else resources.getString(R.string.something_went_wrong)
                            }
                        }else{
                            rl_error.visibility = View.VISIBLE
                            tv_error_message.text = resources.getString(R.string.something_went_wrong)
                        }
            })
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }


    /*
     *
     * method to set View data
     */
    private fun setViewData() {
        val user = getGson().fromJson(PrefUtils.getString(this@DashboardActivity,
                Constants.PREF_USER_DETAILS, null), UserModel::class.java)

        user.apply {
            tv_username.text = "Welcome, ${firstName} ${lastName}"
        }
    }


    /*
     *
     * method to set view props
     * ... listeners, adapters, layoutmanagers
     */
    private fun setViewsProps() {
        btn_logout.setOnClickListener(this@DashboardActivity)
        btn_retry.setOnClickListener(this@DashboardActivity)

        tabs_layout.addTab(tabs_layout.newTab().setText(getString(R.string.my_feed)))
        tabs_layout.addTab(tabs_layout.newTab().setText(getString(R.string.my_posts)))
        tabs_layout.setTabTextColors(Color.parseColor("#ffffff"), Color.parseColor("#ffffff"))
        tabs_layout.setSelectedTabIndicatorColor(ContextCompat.getColor(this@DashboardActivity, R.color.teal_700))

        tabs_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                if(p0?.position == 0){
                    rv_my_feed.visibility = View.VISIBLE
                    rv_my_posts.visibility = View.GONE

                    if(myFeedsList.isEmpty()){
                        rl_error.visibility = View.VISIBLE
                        tv_error_message.text = resources.getString(R.string.no_data_available)
                    }else{
                        rl_error.visibility = View.GONE
                    }
                }

                if(p0?.position == 1){
                    rv_my_posts.visibility = View.VISIBLE
                    rv_my_feed.visibility = View.GONE

                    if(myPostsList.isEmpty()){
                        rl_error.visibility = View.VISIBLE
                        tv_error_message.text = resources.getString(R.string.no_data_available)
                    }else{
                        rl_error.visibility = View.GONE
                    }
                }
            }
        })

        tabs_layout.getTabAt(0)

        pAdapter = PostsAndFeedsAdapter(this@DashboardActivity, PostType.MY_POST)
        fAdapter = PostsAndFeedsAdapter(this@DashboardActivity, PostType.MY_FEED)

        pAdapter.setClickListener(listener = postClickListener)
        fAdapter.setClickListener(listener = postClickListener)

        rv_my_feed.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_my_feed.adapter = fAdapter

        rv_my_posts.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_my_posts.adapter = pAdapter
    }


    /*
     *
     * on Click listener
     */
    override fun onClick(v: View?) {
        try{
            when(v?.id){
                R.id.btn_logout -> {
                    logout(this@DashboardActivity)
                }

                R.id.btn_retry -> {
                    fetchFeedData()
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }


    /*
     *
     * Recycler View Item Click Listener Implementation
     */
    var postClickListener = object : PostClickListener{
        override fun onPostClicked(postType: PostType, positon: Int) {
            when(postType){
                PostType.MY_FEED -> {
                    val postId = myFeedsList[positon].postId
                    startActivity(Intent(this@DashboardActivity, DetailActivity::class.java)
                            .putExtra(Constants.POST_ID, postId))
                }

                PostType.MY_POST -> {
                    val postId = myPostsList[positon].postId
                    startActivity(Intent(this@DashboardActivity, DetailActivity::class.java)
                            .putExtra(Constants.POST_ID, postId))
                }
            }
        }
    }
}