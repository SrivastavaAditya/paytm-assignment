package com.example.simplifiedlinkedin.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.example.simplifiedlinkedin.MyApplication
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.activities.LoginActivity
import com.example.simplifiedlinkedin.dialogs.AlertDialogBuilder
import com.example.simplifiedlinkedin.interfaces.AlertDialogListener
import com.google.gson.Gson
import java.lang.Exception


/*
 *
 * method to logout
 */
fun logout(context: Context){
    AlertDialogBuilder.getDialog(context)
            .setDialogTitle(context.resources.getString(R.string.alert))
            .setDialogMessage(context.resources.getString(R.string.are_you_sure_you_want_to_logout))
            .setDialogPositiveBtnText(context.resources.getString(R.string.yes_logout))
            .setDialogNegativeBtnText(context.resources.getString(R.string.no))
            .setDialogListener(object : AlertDialogListener {
                override fun onOptionClicked(requestCode: Int) {
                    when(requestCode){
                        Constants.REQUEST_NEGATIVE -> {
                            //do nothing
                        }

                        Constants.REQUEST_LOGOUT -> {
                            PrefUtils.putBoolean(context, Constants.PREF_LOGGED_IN, false)
                            PrefUtils.removePref(context, Constants.PREF_USER_DETAILS)
                            context.startActivity(Intent(context, LoginActivity::class.java))
                            (context as Activity).finish()
                        }
                    }
                }

            })
            .buildLogoutDialog()
}


/*
 *
 * method to get gson
 */
fun getGson(): Gson = Gson()


/*
 *
 * method to show Toast message
 */
fun showToastMessage(message: String, time: Int){
    Toast.makeText(MyApplication.getInstance(), message, time).show()
}


/*
 *
 * Extension method to check if intent contains detail
 */
fun Intent.containsValue(key: String): Boolean{
    if(this != null && this.hasExtra(key) && !TextUtils.isEmpty(this.extras?.get(key).toString())){
        return true
    }
    return false
}


/*
 *
 * method to check internet connection
 */
fun isInternetConnected(): Boolean {
    try{
        val cm = MyApplication.getInstance()!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }catch (e: Exception){
        e.printStackTrace()
        Log.e("Utils", e.message.toString())
    }
    return false
}