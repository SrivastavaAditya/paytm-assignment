package com.example.simplifiedlinkedin.network

import com.example.simplifiedlinkedin.models.FeedResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/*
 *
 * Dashboard Retrofit Interface
 */
interface DashboardRetrofitInterface {

    /*
     *
     * network service to fetch feed data
     */
    @GET
    public fun getFeedData(@Url url: String): Call<FeedResponse>
}