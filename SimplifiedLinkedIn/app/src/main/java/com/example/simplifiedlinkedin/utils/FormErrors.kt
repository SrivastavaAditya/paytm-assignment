package com.example.simplifiedlinkedin.utils

/*
 *
 * Form Errors
 */
enum class FormErrors {
    MOBILE_EMPTY,
    MOBILE_STARTS_WITH_0,
    MOBILE_LENGTH_LESS_THAN_10,
    MOBILE_NOT_NUMERICAL,
    PASSWORD_SHORT,
    PASSWORD_EMPTY,
    NO_ERROR,
}