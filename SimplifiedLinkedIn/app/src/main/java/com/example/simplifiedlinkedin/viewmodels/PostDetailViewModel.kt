package com.example.simplifiedlinkedin.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.simplifiedlinkedin.models.PostResponse
import com.example.simplifiedlinkedin.repositories.PostDetailRepository

/*
 *
 * PostDetail View Model
 */
class PostDetailViewModel: ViewModel() {

    private val repository = PostDetailRepository()

    /*
     *
     * method to get post details
     */
    fun getPostDetail(postId: Int): MutableLiveData<PostResponse>{
        return repository.getPostDetail(postId)
    }
}