package com.example.simplifiedlinkedin.repositories

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.simplifiedlinkedin.MyApplication
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.models.LoginResponse
import com.example.simplifiedlinkedin.network.RetrofitClient
import com.example.simplifiedlinkedin.utils.Constants
import com.example.simplifiedlinkedin.utils.isInternetConnected
import com.example.simplifiedlinkedin.utils.showToastMessage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class LoginRepository {

    companion object{
        //Tag
        val TAG = "LoginRepository"
    }


    /*
     *
     * method to request for Login
     */
    public fun requestLogin(mobile: String, password: String): MutableLiveData<LoginResponse>{
        var liveData = MutableLiveData<LoginResponse>()
        try{
            if(isInternetConnected()){
                var URL = Constants.LOGIN_URL.replace("xxx", mobile).replace("yyy", password)
                var call: Call<LoginResponse> = RetrofitClient.getLoginClient().requestLogin(URL)
                call.enqueue(object: Callback<LoginResponse>{
                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        var loginResponse = LoginResponse()
                        loginResponse.status = "FAILED"
                        loginResponse.error = MyApplication.getInstance()!!.resources.getString(R.string.something_went_wrong)
                        liveData.value = loginResponse
                        showToastMessage(t.message.toString(), Toast.LENGTH_SHORT)
                    }

                    override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                        liveData.value = response.body()
                    }
                })
            }else{
                var loginResponse = LoginResponse()
                loginResponse.status = "FAILED"
                loginResponse.error = MyApplication.getInstance()!!.resources.getString(R.string.internet_conection_failed)
                liveData.value = loginResponse
                showToastMessage(MyApplication.getInstance()!!.resources.getString(R.string.internet_conection_failed)
                        , Toast.LENGTH_SHORT)
            }
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
            liveData.postValue(null)
            showToastMessage(MyApplication.getInstance()!!.resources.getString(R.string.something_went_wrong)
                , Toast.LENGTH_SHORT)
        }
        return liveData
    }
}