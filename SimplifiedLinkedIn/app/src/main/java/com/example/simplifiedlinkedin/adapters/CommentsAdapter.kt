package com.example.simplifiedlinkedin.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.models.Comments
import com.example.simplifiedlinkedin.models.UserModel
import com.example.simplifiedlinkedin.utils.Constants
import com.example.simplifiedlinkedin.utils.PrefUtils
import com.example.simplifiedlinkedin.utils.getGson
import kotlinx.android.synthetic.main.item_comment.view.*
import java.lang.Exception

/*
 *
 * Comments Adapter
 */
class CommentsAdapter(val context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        const val TAG = "Comments Adapter"
    }

    var list = arrayListOf<Comments>()
    var currentUser: UserModel? = null

    init {
        currentUser = getGson().fromJson(PrefUtils.getString(context, Constants.PREF_USER_DETAILS, null)
                , UserModel::class.java)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CommentViewHolder(LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val comment = list[position]

        val commonViewHolder = holder as CommentViewHolder

        try{
            currentUser?.apply {
                if (userId == comment.userId) {
                    commonViewHolder.itemView.img_comment_avatar.setImageResource(R.drawable.current_user)
                } else {
                    commonViewHolder.itemView.img_comment_avatar.setImageResource(R.drawable.other_user)
                }
            }

            commonViewHolder.itemView.tv_username.text = comment.owner
            commonViewHolder.itemView.tv_comment.text = comment.content
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }

    class CommentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}