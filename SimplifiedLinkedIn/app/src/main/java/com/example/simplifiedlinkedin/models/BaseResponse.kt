package com.example.simplifiedlinkedin.models

/*
 *
 * Base Response
 */
open class BaseResponse {
    var status: String = ""
    var error: String = ""
}