package com.example.simplifiedlinkedin.network

import com.example.simplifiedlinkedin.utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/*
 *
 * Retrofit Client
 */
class RetrofitClient {

    companion object{
        const val TAG = "RetrofitClient"

        /*
         *
         * method to get OkHttp Client
         */
        private fun getOkHttpClient(): OkHttpClient{
            return OkHttpClient.Builder().build()
        }


        /*
         *
         * method to get Login Retrofit Client
         */
        fun getLoginClient(): LoginRetrofitInterface {
            return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(LoginRetrofitInterface::class.java)
        }


        /*
         *
         * method to get Dashboard Retrofit Client
         */
        fun getDashboardClient(): DashboardRetrofitInterface {
            return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(DashboardRetrofitInterface::class.java)
        }


        /*
         *
         * method to get Post Detail Retrofit Client
         */
        fun getPostDetailClient(): PostDetailRetrofitInterface {
            return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PostDetailRetrofitInterface::class.java)
        }
    }
}