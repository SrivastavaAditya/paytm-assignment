package com.example.simplifiedlinkedin.utils

/*
 *
 * Constants used in apps
 */
object Constants {
    //Request Codes
    const val REQUEST_NEGATIVE = -1
    const val REQUEST_LOGOUT = 101
    const val CREDENTIAL_PICKER_REQUEST = 100
    const val REQUEST_ACTION = 102

    //Intent parameters
    const val POST_ID = "post_id"

    //Shared Preference Keys
    const val PREFERENCE_NAME = "simplified_linkedin_prefs"
    const val PREF_USER_DETAILS = "pref_user_details"
    const val PREF_LOGGED_IN = "pref_logged_in"

    //URL Endpoints
    const val BASE_URL = "https://simpliedlinkedin.free.beeceptor.com"
    val LOGIN_URL = "${BASE_URL}/login?mobile=xxx&password=yyy"
    val DASHBOARD_URL = "$BASE_URL/dashboard"
    val POST_URL = "$BASE_URL/post?userId=xxx&postId=yyy"
}