package com.example.simplifiedlinkedin.dialogs

/*
 *
 * dialog building components
 */
class DialogComponents {
    var title: String? = null
    var message: String? = null
    var positiveBtnText: String? = null
    var negativeBtnText: String? = null
}