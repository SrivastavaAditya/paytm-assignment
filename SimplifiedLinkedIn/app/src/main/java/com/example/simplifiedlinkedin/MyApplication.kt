package com.example.simplifiedlinkedin

import android.app.Application

/*
 *
 * MyApplication
 */
class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    companion object{
        lateinit var INSTANCE: MyApplication

        /*
         *
         * method to get MyApplication Instance
         */
        @Synchronized
        fun getInstance(): MyApplication? {
            return INSTANCE
        }
    }
}