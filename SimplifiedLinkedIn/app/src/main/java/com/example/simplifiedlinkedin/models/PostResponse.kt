package com.example.simplifiedlinkedin.models

/*
 *
 * Post Response
 */
class PostResponse: BaseResponse() {
    val data: Post? = null
}