package com.example.simplifiedlinkedin.network

import com.example.simplifiedlinkedin.models.PostResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/*
 *
 * Post Detail Retrofit Interface
 */
interface PostDetailRetrofitInterface{

    /*
     *
     * network service to to fetch post details
     */
    @GET
    public fun getPostDetails(@Url url: String): Call<PostResponse>
}