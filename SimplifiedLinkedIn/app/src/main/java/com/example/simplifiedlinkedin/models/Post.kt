package com.example.simplifiedlinkedin.models

/*
 *
 * Posts POJO
 */
class Post {
    var postId: Int = 0
    var owner: String = ""
    var userId: Long = 0
    var description: String = ""
    var comments = arrayListOf<Comments>()
}