package com.example.simplifiedlinkedin.dialogs

import android.app.Dialog
import android.content.Context
import android.os.CountDownTimer
import android.text.TextUtils
import android.view.View
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.interfaces.AlertDialogListener
import com.example.simplifiedlinkedin.utils.Constants
import kotlinx.android.synthetic.main.dialog_logout.*


/*
 *
 * Alert Dialog Builder
 */
class AlertDialogBuilder(val context: Context) {

    var dialogComponents: DialogComponents? = null
    var listener: AlertDialogListener? = null

    init{
        dialogComponents = DialogComponents()
    }

    companion object{
        const val TAG = "Dialog"

        lateinit var INSTANCE: AlertDialogBuilder

        public fun getDialog(context: Context): AlertDialogBuilder{
            INSTANCE = AlertDialogBuilder(context)
            return INSTANCE
        }
    }

    /*
     *
     * method to set dialog title
     */
    public fun setDialogTitle(title: String): AlertDialogBuilder{
        dialogComponents?.title = title
        return INSTANCE
    }

    /*
     *
     * method to set dialog message
     */
    public fun setDialogMessage(message: String): AlertDialogBuilder{
        dialogComponents?.message = message
        return INSTANCE
    }

    /*
     *
     * method to set dialog positive button text
     */
    public fun setDialogPositiveBtnText(positive: String): AlertDialogBuilder{
        dialogComponents?.positiveBtnText = positive
        return INSTANCE
    }

    /*
     *
     * method to set dialog negative button text
     */
    public fun setDialogNegativeBtnText(negative: String): AlertDialogBuilder{
        dialogComponents?.negativeBtnText = negative
        return INSTANCE
    }

    /*
     *
     * method to set dialog listener
     */
    public fun setDialogListener(listener: AlertDialogListener): AlertDialogBuilder{
        this.listener = listener
        return INSTANCE
    }


    /*
     *
     * method to build logout dialog
     */
    public fun buildLogoutDialog(): AlertDialogBuilder{
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(R.layout.dialog_logout)

        dialogComponents?.apply {
            dialog.tv_title.text = if(!TextUtils.isEmpty(title)) title else context.resources.getString(R.string.alert)
            dialog.tv_message.text = if(!TextUtils.isEmpty(message)) message else context.resources.getString(R.string.are_you_sure_you_want_to_logout)

            if(!TextUtils.isEmpty(dialogComponents?.positiveBtnText)){
                dialog.btn_positive.visibility = View.VISIBLE
                dialog.btn_positive.text = dialogComponents?.positiveBtnText
            }else{
                dialog.btn_positive.visibility = View.GONE
            }

            if(!TextUtils.isEmpty(dialogComponents?.negativeBtnText)){
                dialog.btn_negative.visibility = View.VISIBLE
                dialog.btn_negative.text = dialogComponents?.negativeBtnText
            }else{
                dialog.btn_negative.visibility = View.GONE
            }

        }

        if(dialog.btn_positive.visibility == View.VISIBLE){
            dialog.btn_positive.setOnClickListener {
                dialog.loading.visibility = View.VISIBLE
                object : CountDownTimer(1000, 100){
                    override fun onFinish() {
                        listener?.onOptionClicked(Constants.REQUEST_LOGOUT)
                        dialog.dismiss()
                    }

                    override fun onTick(millisUntilFinished: Long) {
                    }

                }.start()
            }
        }

        if(dialog.btn_negative.visibility == View.VISIBLE){
            dialog.btn_negative.setOnClickListener {
                listener?.onOptionClicked(Constants.REQUEST_NEGATIVE)
                dialog.dismiss()
            }
        }

        dialog.show()
        return INSTANCE
    }


    /*
     *
     * method to build default dialog
     */
    public fun build(): AlertDialogBuilder{
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(R.layout.default_dialog)

        dialogComponents?.apply {
            dialog.tv_title.text = if(!TextUtils.isEmpty(title)) title else context.resources.getString(R.string.alert)
            dialog.tv_message.text = if(!TextUtils.isEmpty(message)) message else context.resources.getString(R.string.something_went_wrong)

            if(!TextUtils.isEmpty(dialogComponents?.positiveBtnText)){
                dialog.btn_positive.visibility = View.VISIBLE
                dialog.btn_positive.text = dialogComponents?.positiveBtnText
            }else{
                dialog.btn_positive.visibility = View.GONE
            }

            if(!TextUtils.isEmpty(dialogComponents?.negativeBtnText)){
                dialog.btn_negative.visibility = View.VISIBLE
                dialog.btn_negative.text = dialogComponents?.negativeBtnText
            }else{
                dialog.btn_negative.visibility = View.GONE
            }
        }

        if(dialog.btn_positive.visibility == View.VISIBLE){
            dialog.btn_positive.setOnClickListener {
                listener?.onOptionClicked(Constants.REQUEST_ACTION)
                dialog.dismiss()
            }
        }

        if(dialog.btn_negative.visibility == View.VISIBLE){
            dialog.btn_negative.setOnClickListener {
                listener?.onOptionClicked(Constants.REQUEST_NEGATIVE)
                dialog.dismiss()
            }
        }

        dialog.show()
        return INSTANCE
    }

}