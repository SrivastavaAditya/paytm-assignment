package com.example.simplifiedlinkedin.models

/*
 *
 * Login Response
 */
class LoginResponse: BaseResponse() {
    var data: UserModel? = null
}