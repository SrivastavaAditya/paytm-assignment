package com.example.simplifiedlinkedin.interfaces

/*
 *
 * Alert Dialog Listener
 */
interface AlertDialogListener {
    
    fun onOptionClicked(requestCode: Int)
}