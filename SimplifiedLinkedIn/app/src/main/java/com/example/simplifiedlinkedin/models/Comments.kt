package com.example.simplifiedlinkedin.models

/*
 *
 * Comments POJO
 */
class Comments {

    var owner: String = ""
    var content: String = ""
    var userId: Long = 0
}