package com.example.simplifiedlinkedin.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.simplifiedlinkedin.models.LoginResponse
import com.example.simplifiedlinkedin.repositories.LoginRepository

/*
 *
 * Login View Model
 */
class LoginViewModel: ViewModel() {

    private val repository = LoginRepository()

    /*
     *
     * method to request for login
     */
    public fun requestLogin(mobile: String, password: String): MutableLiveData<LoginResponse>{
        return repository.requestLogin(mobile, password)
    }
}