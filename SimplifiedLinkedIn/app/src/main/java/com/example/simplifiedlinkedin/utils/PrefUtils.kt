package com.example.simplifiedlinkedin.utils

import android.content.Context
import android.content.SharedPreferences
import android.util.Log

/*
 *
 * Pref Utils
 */
object PrefUtils {

    const val TAG = "PrefUtils"

    /*
     *
     * method to get shared preferences
     */
    public fun getSharedPreferences(context: Context): SharedPreferences{
        return context.getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    /*
     *
     * method to save boolean value
     */
    public fun putBoolean(context: Context, key: String, value: Boolean){
        val sharedPreferences = getSharedPreferences(context)
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    /*
     *
     * method to save string value
     */
    public fun putString(context: Context, key: String, value: String){
        val sharedPreferences = getSharedPreferences(context)
        sharedPreferences.edit().putString(key, value).apply();
    }

    /*
     *
     * method to save integer value
     */
    public fun putInt(context: Context, key: String, value: Int){
        val sharedPreferences = getSharedPreferences(context)
        sharedPreferences.edit().putInt(key, value).apply();
    }

    /*
     *
     * method to get boolean value
     */
    public fun getBoolean(context: Context, key: String, defaultValue: Boolean): Boolean{
        val sharedPreferences = getSharedPreferences(context)
        return sharedPreferences.getBoolean(key, defaultValue)
    }

    /*
     *
     * method to get string value
     */
    public fun getString(context: Context, key: String, defaultValue: String?): String? {
        val sharedPreferences = getSharedPreferences(context)
        return sharedPreferences.getString(key, defaultValue)
    }

    /*
     *
     * method to get int value
     */
    public fun getInt(context: Context, key: String, defaultValue: Int): Int{
        val sharedPreferences = getSharedPreferences(context)
        return sharedPreferences.getInt(key, defaultValue)
    }

    /*
     *
     * method to check is key exists in preferences
     */
    public fun isKeyPresentInPref(context: Context, key: String): Boolean{
        val sharedPreferences = getSharedPreferences(context)
        return sharedPreferences.contains(key)
    }

    /*
     *
     * method to remove saved preference
     */
    public fun removePref(context: Context, key: String){
        try{
            val sharedPreferences = getSharedPreferences(context)
            sharedPreferences.edit().remove(key).apply()
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }


}