package com.example.simplifiedlinkedin.models

/*
 *
 * User POJO
 */
class UserModel {
    var firstName: String = ""
    var lastName: String = ""
    var userId: Long = 0;
}