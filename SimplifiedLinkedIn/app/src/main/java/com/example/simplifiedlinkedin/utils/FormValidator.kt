package com.example.simplifiedlinkedin.utils


/*
 *
 * Form Validator
 */
object FormValidator {

    /*
     *
     * check if mobile no starts with 0
     */
    fun isMobileNumberStartingWith0(mobile: String): Boolean{
        return mobile[0] == '0'
    }

    /*
     *
     * check if mobile no less than 10 characters
     */
    fun isMobileNumberLengthLessThan10(mobile: String): Boolean{
        return mobile.isNotEmpty() && mobile.length<10
    }

    /*
     *
     * check if mobile no empty
     */
    fun isMobileNumberEmpty(mobile: String): Boolean{
        return mobile.isEmpty()
    }

    /*
     *
     * check if mobile no not numerical
     */
    fun isMobileNumberNotNumerical(mobile: String): Boolean{
        for(i in 0..mobile.length-1){
            if(!mobile[i].isDigit()){
                return true
            }
        }
        return false
    }

    /*
     *
     * check if password length less than 4 characters
     */
    fun isPasswordLengthShort(password: String): Boolean{
        return password.isNotEmpty() && password.length<4
    }

    /*
     *
     * check if password empty
     */
    fun isPasswordEmpty(password: String): Boolean{
        return password.isEmpty()
    }
    

    /*
     *
     * method to validate user inputs
     */
    fun isInputValid(mobile: String, password: String): FormErrors{
        if(isMobileNumberEmpty(mobile)){
            return FormErrors.MOBILE_EMPTY    
        }

        if(isMobileNumberStartingWith0(mobile)){
            return FormErrors.MOBILE_STARTS_WITH_0
        }

        if(isMobileNumberLengthLessThan10(mobile)){
            return FormErrors.MOBILE_LENGTH_LESS_THAN_10
        }
        
        if(isMobileNumberNotNumerical(mobile)){
            return FormErrors.MOBILE_NOT_NUMERICAL
        }
        
        if(isPasswordEmpty(password)){
            return FormErrors.PASSWORD_EMPTY   
        }
        
        if(isPasswordLengthShort(password)){
            return FormErrors.PASSWORD_SHORT
        }
        
        return FormErrors.NO_ERROR
    }
}