package com.example.simplifiedlinkedin.repositories

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.simplifiedlinkedin.MyApplication
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.models.*
import com.example.simplifiedlinkedin.network.RetrofitClient
import com.example.simplifiedlinkedin.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PostDetailRepository {

    companion object{
        const val TAG = "PostDetailRepository"
    }


    /*
     *
     * method to fetch post details
     */
    fun getPostDetail(postId: Int): MutableLiveData<PostResponse>{
        var liveData = MutableLiveData<PostResponse>()

        try{
            if(isInternetConnected()){
                val currentUser = getGson().fromJson(PrefUtils.getString(MyApplication.getInstance()!!,
                        Constants.PREF_USER_DETAILS, null)
                        , UserModel::class.java)
                val URL = Constants.POST_URL.replace("xxx", currentUser.userId.toString())
                        .replace("yyy", postId.toString())

                val call: Call<PostResponse> = RetrofitClient.getPostDetailClient().getPostDetails(URL)
                call.enqueue(object : Callback<PostResponse>{
                    override fun onFailure(call: Call<PostResponse>, t: Throwable) {
                        val postResponse = PostResponse()
                        postResponse.status = "FAILED"
                        postResponse.error = MyApplication.getInstance()!!.resources.getString(R.string.something_went_wrong)
                        liveData.value = postResponse
                        showToastMessage(t.message.toString(), Toast.LENGTH_SHORT)
                    }

                    override fun onResponse(call: Call<PostResponse>, response: Response<PostResponse>) {
                        liveData.value = response.body()
                    }
                })
            }else{
                var postResponse = PostResponse()
                postResponse.status = "FAILED"
                postResponse.error = MyApplication.getInstance()!!.resources.getString(R.string.internet_conection_failed)
                liveData.value = postResponse
                showToastMessage(MyApplication.getInstance()!!.resources.getString(R.string.internet_conection_failed)
                        , Toast.LENGTH_SHORT)
            }
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
            liveData.value = null
            showToastMessage(MyApplication.getInstance()!!.resources.getString(R.string.something_went_wrong)
                , Toast.LENGTH_SHORT)
        }

        return liveData
    }
}