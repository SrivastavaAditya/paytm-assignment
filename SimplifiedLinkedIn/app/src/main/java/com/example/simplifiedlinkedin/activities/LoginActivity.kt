package com.example.simplifiedlinkedin.activities

import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.dialogs.AlertDialogBuilder
import com.example.simplifiedlinkedin.dialogs.LoadingDialog
import com.example.simplifiedlinkedin.models.LoginResponse
import com.example.simplifiedlinkedin.utils.*
import com.example.simplifiedlinkedin.viewmodels.LoginViewModel
import com.google.android.gms.auth.api.credentials.*
import kotlinx.android.synthetic.main.activity_login.*


/*
 *
 * LoginActivity
 */
class LoginActivity : AppCompatActivity(), View.OnClickListener {

    companion object{
        const val TAG = "LoginActivity"
    }

    lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        setViews()

        requestMobileNumberFromDevice()
    }


    /*
     *
     * method to set views properties
     * ... listeners
     */
    private fun setViews() {
        btn_login.setOnClickListener(this@LoginActivity)
        img_hide_unhide.setOnClickListener(this@LoginActivity)

        et_mobile_no.addTextChangedListener(textWatcher)

        et_password.addTextChangedListener(textWatcher)
    }


    /*
     *
     * method to remove errors
     */
    private fun removeErrors() {
        mobile_error.text = ""
        password_error.text = ""
        mobile_error.visibility = View.INVISIBLE
        password_error.visibility = View.INVISIBLE
    }


    /*
     *
     * method to request mobile number from device
     */
    private fun requestMobileNumberFromDevice(){
        try {
            val hintRequest = HintRequest.Builder()
                    .setHintPickerConfig(CredentialPickerConfig.Builder()
                            .setShowCancelButton(true)
                            .build())
                    .setPhoneNumberIdentifierSupported(true)
                    .build()

            val options = CredentialsOptions.Builder()
                    .forceEnableSaveDialog()
                    .build()

            val credentialsClient = Credentials.getClient(applicationContext, options)
            val intent: PendingIntent = credentialsClient.getHintPickerIntent(hintRequest)
            startIntentSenderForResult(intent.intentSender, Constants.CREDENTIAL_PICKER_REQUEST,
                    null, 0, 0, 0)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }


    /*
     *
     * activity result
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try{
            if(resultCode == Activity.RESULT_OK){
                if(requestCode == Constants.CREDENTIAL_PICKER_REQUEST){
                    val credential: Credential? = data?.getParcelableExtra(Credential.EXTRA_KEY)
                    credential?.apply {
                        if(id.contains("+")){
                            et_mobile_no.setText(credential.id.replace("+91", ""))
                        }else{
                            et_mobile_no.setText(credential.id)
                        }
                    }
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    /*
     *
     * view clicks
     */
    override fun onClick(v: View?) {
        try{
            when(v?.id){
                R.id.img_hide_unhide -> {
                    changePasswordTextVisibility()
                }

                R.id.btn_login -> {
                    if(validInput()){
                        requestLogin()
                    }
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }


    /*
     *
     * method to request for login
     */
    private fun requestLogin() {
        try{
            val dialog = LoadingDialog.getInstance(this@LoginActivity)
            dialog.show()
            loginViewModel.requestLogin(et_mobile_no.text.toString(), et_password.text.toString())
                    .observe(this@LoginActivity, Observer<LoginResponse> {
                        dialog.dismiss()
                        if(it != null){
                            if("SUCCESS".equals(it.status, ignoreCase = true) && it.data != null){
                                PrefUtils.putString(this@LoginActivity, Constants.PREF_USER_DETAILS,
                                        getGson().toJson(it.data))
                                PrefUtils.putBoolean(this@LoginActivity, Constants.PREF_LOGGED_IN, true)
                                startActivity(Intent(this@LoginActivity, DashboardActivity::class.java))
                                finish()
                            }else{
                                AlertDialogBuilder.getDialog(this@LoginActivity)
                                        .setDialogTitle(resources.getString(R.string.alert))
                                        .setDialogMessage(if(!TextUtils.isEmpty(it.error)) it.error else resources.getString(R.string.something_went_wrong))
                                        .setDialogPositiveBtnText(resources.getString(R.string.try_again))
                                        .build()
                            }
                        }else {
                            AlertDialogBuilder.getDialog(this@LoginActivity)
                                    .setDialogTitle(resources.getString(R.string.alert))
                                    .setDialogMessage(resources.getString(R.string.something_went_wrong))
                                    .setDialogPositiveBtnText(resources.getString(R.string.try_again))
                                    .build()
                        }
                    })
        }catch (e: java.lang.Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }


    /*
     *
     * method to validate input and return error
     */
    private fun validInput(): Boolean {
        val error = FormValidator.isInputValid(et_mobile_no.text.toString(), et_password.text.toString())
        when(error){
            FormErrors.MOBILE_EMPTY -> {
                mobile_error.visibility = View.VISIBLE
                mobile_error.text = resources.getString(R.string.mobile_no_is_mandatory)
                return false
            }

            FormErrors.MOBILE_STARTS_WITH_0 -> {
                mobile_error.visibility = View.VISIBLE
                mobile_error.text = resources.getString(R.string.mobile_no_cannot_start_with_zero)
                return false
            }

            FormErrors.MOBILE_NOT_NUMERICAL -> {
                mobile_error.visibility = View.VISIBLE
                mobile_error.text = resources.getString(R.string.mobile_no_should_be_numeric)
                return false
            }

            FormErrors.MOBILE_LENGTH_LESS_THAN_10 -> {
                mobile_error.visibility = View.VISIBLE
                mobile_error.text = resources.getString(R.string.mobile_no_should_be_ten_characters)
                return false
            }

            FormErrors.PASSWORD_EMPTY -> {
                password_error.visibility = View.VISIBLE
                password_error.text = resources.getString(R.string.password_canot_be_empty)
                return false
            }

            FormErrors.PASSWORD_SHORT -> {
                password_error.visibility = View.VISIBLE
                password_error.text = resources.getString(R.string.password_should_be_atleast_four_character)
                return false
            }

            FormErrors.NO_ERROR -> {
                return true
            }
        }
    }


    /*
     *
     * changes password text visibility
     * encrypted text to normal text and vice-versa
     */
    private fun changePasswordTextVisibility() {
        try{
            when(et_password.transformationMethod){
                PasswordTransformationMethod.getInstance() -> {
                    et_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    img_hide_unhide.setImageResource(R.drawable.baseline_visibility_black_18)
                }

                HideReturnsTransformationMethod.getInstance() -> {
                    et_password.transformationMethod = PasswordTransformationMethod.getInstance()
                    img_hide_unhide.setImageResource(R.drawable.baseline_visibility_off_black_18)
                }
            }
            et_password.setSelection(et_password.length());
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }


    /*
     *
     * common textwatcher for editTexts
     */
    val textWatcher = object : TextWatcher{
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            removeErrors()
        }

    }


    override fun onBackPressed() {
        finishAffinity();
        finish();
    }
}