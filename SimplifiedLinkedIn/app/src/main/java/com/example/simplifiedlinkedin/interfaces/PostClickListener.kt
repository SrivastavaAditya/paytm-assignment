package com.example.simplifiedlinkedin.interfaces

import com.example.simplifiedlinkedin.utils.PostType

/*
 *
 * Recycler View Item Click Listener
 */
interface PostClickListener {

    public fun onPostClicked(postType: PostType, positon: Int)
}