package com.example.simplifiedlinkedin.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.simplifiedlinkedin.MyApplication
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.adapters.CommentsAdapter
import com.example.simplifiedlinkedin.dialogs.LoadingDialog
import com.example.simplifiedlinkedin.models.UserModel
import com.example.simplifiedlinkedin.utils.*
import com.example.simplifiedlinkedin.viewmodels.PostDetailViewModel
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_detail.btn_logout
import kotlinx.android.synthetic.main.layout_no_data.*
import java.lang.Exception

/*
 *
 * DetailActivity
 */
class DetailActivity : AppCompatActivity(), View.OnClickListener {

    companion object{
        const val TAG = "DetailActivity"

        val currentUser: UserModel = getGson().fromJson(PrefUtils.getString(MyApplication.getInstance()!!,
                Constants.PREF_USER_DETAILS, null), UserModel::class.java)
    }

    lateinit var cAdapter: CommentsAdapter
    lateinit var postDetailViewModel: PostDetailViewModel

    var postId: Int = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        postDetailViewModel = ViewModelProvider(this@DetailActivity).get(PostDetailViewModel::class.java)

        if(intent.containsValue(Constants.POST_ID)){
            postId = intent.extras!!.getInt(Constants.POST_ID)
        }

        setViewProps()

        fetchPost()
    }


    /*
     *
     * method to fetch selected post details
     */
    private fun fetchPost() {
        try{
            val dialog = LoadingDialog.getInstance(this@DetailActivity)
            dialog.show()
            postDetailViewModel.getPostDetail(postId).observe(this@DetailActivity, Observer {
                dialog.dismiss()
                if(it != null){
                    if("SUCCESS".equals(it.status, ignoreCase = true) && TextUtils.isEmpty(it.error)){
                        if(it.data != null){
                            rl_error.visibility = View.GONE
                            if(currentUser.userId == it.data.userId){
                                img_avatar.setImageResource(R.drawable.current_user)
                            }else{
                                img_avatar.setImageResource(R.drawable.other_user)
                            }

                            tv_username.text = it.data.owner
                            tv_description.text = it.data.description

                            val commentsCount = if(it.data.comments.isNotEmpty()) it.data.comments.size else 0
                            if(commentsCount>1){
                                tv_comments.text = "${commentsCount} Comments"
                            }else if(commentsCount == 1){
                                tv_comments.text = resources.getString(R.string.one_comment)
                            }else{
                                tv_comments.text = resources.getString(R.string.no_comments)
                            }

                            if(commentsCount>=1){
                                cAdapter.list.clear()
                                cAdapter.list = it.data.comments
                                cAdapter.notifyDataSetChanged()
                            }else{
                                rv_comments.visibility = View.GONE
                                rl_no_comment.visibility = View.VISIBLE
                            }
                        }else{
                            rl_content.visibility = View.GONE
                            rl_error.visibility = View.VISIBLE
                            tv_error_message.text = resources.getString(R.string.no_data_available)
                        }
                    }else{
                        rl_content.visibility = View.GONE
                        rl_error.visibility = View.VISIBLE
                        tv_error_message.text = if(!TextUtils.isEmpty(it.error)) it.error else resources.getString(R.string.something_went_wrong)
                    }
                }else{
                    rl_content.visibility = View.GONE
                    rl_error.visibility = View.VISIBLE
                    tv_error_message.text = resources.getString(R.string.something_went_wrong)
                }
            })
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }


    /*
     * method to set View Props
     * ... listeners, adapters, layoutmanagers
     */
    private fun setViewProps() {
        btn_logout.setOnClickListener(this@DetailActivity)
        btn_back.setOnClickListener(this@DetailActivity)
        btn_retry.setOnClickListener(this@DetailActivity)

        cAdapter = CommentsAdapter(this@DetailActivity)

        rv_comments.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_comments.adapter = cAdapter
    }


    /*
     *
     * on Click listener
     */
    override fun onClick(v: View?) {
        try{
            when(v?.id){
                R.id.btn_logout -> {
                    logout(this@DetailActivity)
                }

                R.id.btn_back -> {
                    finish()
                }

                R.id.btn_retry -> {
                    fetchPost()
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }
}