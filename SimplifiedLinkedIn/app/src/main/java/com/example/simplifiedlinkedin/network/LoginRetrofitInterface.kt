package com.example.simplifiedlinkedin.network

import com.example.simplifiedlinkedin.models.LoginResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/*
 *
 * Login Retrofit Interface
 */
interface LoginRetrofitInterface {

    /*
     *
     * network service to login
     */
    @GET
    public fun requestLogin(@Url url: String): Call<LoginResponse>
}