package com.example.simplifiedlinkedin.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.utils.Constants
import com.example.simplifiedlinkedin.utils.PrefUtils
import kotlinx.android.synthetic.main.activity_main.*


/*
 *
 * MainActivity - Splash Screen
 */
class MainActivity : AppCompatActivity() {

    companion object{
        const val TAG = "MainActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var firstAnimation = AnimationUtils.loadAnimation(this@MainActivity, R.anim.fade_in)
        val secondAnimation = AnimationUtils.loadAnimation(this@MainActivity, R.anim.fade_in)

        firstAnimation.setAnimationListener(object: Animation.AnimationListener{
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                tv_app_name_first.visibility = View.VISIBLE
                tv_app_name_second.startAnimation(secondAnimation)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })

        secondAnimation.setAnimationListener(object: Animation.AnimationListener{
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                tv_app_name_second.visibility = View.VISIBLE
                Thread.sleep(1000)
                if(PrefUtils.getBoolean(this@MainActivity, Constants.PREF_LOGGED_IN, false)){
                    startActivity(Intent(this@MainActivity, DashboardActivity::class.java))
                }else{
                    startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                }
                finish()
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })

        object : CountDownTimer(1000, 100){
            override fun onFinish() {
                tv_app_name_first.startAnimation(firstAnimation)
            }

            override fun onTick(millisUntilFinished: Long) {

            }
        }.start()
    }


}