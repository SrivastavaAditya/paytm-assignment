package com.example.simplifiedlinkedin.utils

/*
 *
 * Post Types
 */
enum class PostType {
    MY_POST,
    MY_FEED
}