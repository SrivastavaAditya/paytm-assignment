package com.example.simplifiedlinkedin.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.simplifiedlinkedin.models.FeedResponse
import com.example.simplifiedlinkedin.repositories.DashboardRepository

/*
 *
 * Dashboard Viewmodel
 */
class DashboardViewModel: ViewModel() {

    private val repository = DashboardRepository()

    /*
     *
     * method to fetch feed data
     */
    fun getFeedData(): MutableLiveData<FeedResponse>{
        return repository.getFeedData()
    }
}