package com.example.simplifiedlinkedin.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.simplifiedlinkedin.R
import com.example.simplifiedlinkedin.interfaces.PostClickListener
import com.example.simplifiedlinkedin.models.Post
import com.example.simplifiedlinkedin.utils.PostType
import kotlinx.android.synthetic.main.item_post.view.*
import java.lang.Exception


/*
 *
 * Posts and Feeds Adapter
 */
class PostsAndFeedsAdapter(private val context: Context, val postType: PostType): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var list = arrayListOf<Post>()
    var listener: PostClickListener? = null

    companion object{
        const val TAG = "PostsAndFeedsAdapter"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PostViewHolder(LayoutInflater.from(context).inflate(R.layout.item_post, parent, false))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val post = list[position]
        val postHolder = holder as PostViewHolder

        try{
            when(postType){
                PostType.MY_POST -> {
                    postHolder.itemView.img_avatar.setImageResource(R.drawable.current_user)
                }

                PostType.MY_FEED -> {
                    postHolder.itemView.img_avatar.setImageResource(R.drawable.other_user)
                }
            }

            postHolder.itemView.tv_username.text = post.owner
            postHolder.itemView.tv_description.text = post.description

            val commentsCount = if(post.comments.isNotEmpty()) post.comments.size else 0
            if(commentsCount>1){
                postHolder.itemView.tv_comments.text = "${commentsCount} Comments"
            }else if(commentsCount == 1){
                postHolder.itemView.tv_comments.text = context.resources.getString(R.string.one_comment)
            }else{
                postHolder.itemView.tv_comments.text = context.resources.getString(R.string.no_comments)
            }

            postHolder.itemView.cv_parent.setOnClickListener{
                listener?.onPostClicked(postType, position)
            }
        }catch (e: Exception){
            e.printStackTrace()
            Log.e(TAG, e.message.toString())
        }
    }


    fun setClickListener(listener: PostClickListener){
        this.listener = listener
    }

    class PostViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}