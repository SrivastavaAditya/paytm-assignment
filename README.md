# README #
There is  short assignment as a part of evaluation process for Paytm.


### What is this repository for? ###
* Quick summary - 
	The application is an attempt to create a Simplified version of LinkedIn Mobile App. 
* Version 1.0


### How do I get set up? ###
* Summary of set up -
	1. Clone project onto system using command - git clone https://SrivastavaAditya@bitbucket.org/SrivastavaAditya/paytm-assignment.git
	2. Mock Services using https://beeceptor.com/
	3. Run Application on android emulator/device
	
	
### How to Mock Network Services guidelines ###
1. Create endpoint on https://beeceptor.com/ "simpliedlinkedin". It will automatically generate a base endpoint "https://simpliedlinkedin.free.beeceptor.com"
   and you will be redirected to https://beeceptor.com/console/simpliedlinkedin
   
2. Click on highlighted text (or click here to simulate in web-browser) on the screen. You will now have option to mock services with 2 default urls.

3. To create services select any 1 url, a window will open to mock services.

4. For Login Service -
	a. select GET and write url in this format - /login?mobile=9988776655&password=1234 
	b. in response body field add the following response:-
		{
			"status": "SUCCESS",
			"data": {
				"firstName": "Aditya",
				"lastName": "Srivastava",
				"userId": 9988776655
			}
		}
	c. Save Rule
	
5. For Dashboard Service -
	a. select GET and write url in this format - /dashboard
	b. in response body field add the following response:- 
		i. Positive Response :-
			{
			  "status": "SUCCESS",
			  "data": [
				{
				  "postId": 1,
				  "owner": "Aditi Srivastava",
				  "userId": 9998887776,
				  "description": "Hello, this is a post from Aditi Srivastava",
				  "comments": [
					{
					  "owner": "Aditi Srivastava",
					  "userId": 9998887776,
					  "content": "Hello, this is a comment from Aditi Srivastava"
					},
					{
					  "owner": "Aprajeeta Srivastava",
					  "userId": 9999888877,
					  "content": "Hello, this is a comment from Aprajeeta Srivastava"
					},
					{
					  "owner": "Aditya Srivastava",
					  "userId": 9988776655,
					  "content": "Hello, this is a comment from Aditya Srivastava"
					}
				  ]
				},
				{
				  "postId": 2,
				  "owner": "Aprajeeta Srivastava",
				  "userId": 9999888877,
				  "description": "Hello, this is a post from Aprajeeta Srivastava",
				  "comments": [
					{
					  "owner": "Aditi Srivastava",
					  "userId": 9998887776,
					  "content": "Hello, this is a comment from Aprajeeta Srivastava"
					},
					{
					  "owner": "Aprajeeta Srivastava",
					  "userId": 9999888877,
					  "content": "Hello, this is a comment from Aprajeeta Srivastava"
					},
					{
					  "owner": "Aditya Srivastava",
					  "userId": 9988776655,
					  "content": "Hello, this is a comment from Aditya Srivastava"
					}
				  ]
				},
				{
				  "postId": 3,
				  "owner": "Aditya Srivastava",
				  "userId": 9988776655,
				  "description": "Hello, this is a post from Aditya Srivastava",
				  "comments": [
					{
					  "owner": "Aditi Srivastava",
					  "userId": 9998887776,
					  "content": "Hello, this is a comment from Aditi Srivastava"
					},
					{
					  "owner": "Aprajeeta Srivastava",
					  "userId": 9999888877,
					  "content": "Hello, this is a comment from Aprajeeta Srivastava"
					},
					{
					  "owner": "Aditya Srivastava",
					  "userId": 9988776655,
					  "content": "Hello, this is a comment from Aditya Srivastava"
					}
				  ]
				}
			  ]
			}
		
		ii. Error/Negative Responses :-
			{
				"status": "SUCCESS",
				"data": []	
			}
			AND
			{
				"status": "FAILED",
				"error": "Server Error"
			}

	c. Save Rule
	
6. For Selected Post Detail Service:-
	a. select GET and write url in this format - /post?userId=9953110874&postId=1
	b. 	b. in response body field add the following response:- 
		i. Positive Response :-
		{
		  "status": "SUCCESS",
		  "data": {
			"postId": 1,
			"owner": "Aditi Srivastava",
			"userId": 9998887776,
			"description": "Hello, this is a post from Aditi Srivastava",
			"comments": [
			  {
				"owner": "Aditi Srivastava",
				"userId": 9998887776,
				"content": "Hello, this is a comment from Aditi Srivastava"
			  },
			  {
				"owner": "Aprajeeta Srivastava",
				"userId": 9999888877,
				"content": "Hello, this is a comment from Aprajeeta Srivastava"
			  },
			  {
				"owner": "Aditya Srivastava",
				"userId": 9988776655,
				"content": "Hello, this is a comment from Aditya Srivastava"
			  }
			]
		  }
		}
		
		ii. Error/Negative Responses :-
			{
				"status": "FAILED",
				"error": "Server Error"
			}
			
	c. Save Rule
		
		
### Note ###
	Using free version of Mocked Services from https://beeceptor.com/ allows only 50 requests per day.



### Contribution guidelines ###
* Contributer - Aditya Srivastava


### Who do I talk to? ###
* Repo owner - Aditya Srivastava